import json
import os
from tools import logger
from tools import arg_fetcher


class PresentPerson:
    @staticmethod
    def start(js_view_key, local_manager, arguments):
        PresentPerson.action_id = arg_fetcher.get_argument(arguments, 'id')
        if not PresentPerson.action_id:
            logger.log("Missing id in {0} action arguments".format(js_view_key), "Views Manager", logger.ERROR)
            local_manager.send_view_result(js_view_key, {'error': 400})
        
        args = arg_fetcher.get_argument(arguments, 'args')
        speech = arg_fetcher.get_argument(args, 'speech')
        
        text = arg_fetcher.get_argument(speech, 'title')
        said = arg_fetcher.get_argument(speech, 'said')

        who = arg_fetcher.get_argument(args, 'who')
        
        if said:
            if arg_fetcher.get_argument(speech, 'noSpeechAnimated'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", False)
                PresentPerson.reactivateMovement = True
            
            top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_return'])
            PresentPerson.topic_name = local_manager.dialog.loadTopic(top_path)
        
        local_manager.memory.raiseEvent(local_manager.lm_config['currentView']['ALMemory'], json.dumps({
            'view': js_view_key,
            'data': {
                'textToShow': text,
                'people':{
                    'who': who
                }
            }
        }))
        
        if said:

            namewho = arg_fetcher.get_argument(who, 'name')
            imagepathwho = arg_fetcher.get_argument(who, 'image') # TODO USE THIS IMAGE PATH !
            drinkwho = arg_fetcher.get_argument(who, 'drink')

            said = said.format(who1_name=namewho, who1_drink=drinkwho['name'])

            if arg_fetcher.get_argument(speech, 'noSpeechRecognition'):
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['error'],
                    'system': toolbar['system']['micro']
                }))

                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_return'])

                local_manager.dialog.setConcept("saySmthDyn", "enu", [said])

                PresentPerson.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(PresentPerson.topic_name)

                local_manager.dialog.activateTag("saySmthTag", PresentPerson.topic_name)
                local_manager.dialog.gotoTag("saySmthTag", PresentPerson.topic_name)

            else:
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['ok'],
                    'system': toolbar['system']['micro']
                }))

                local_manager.dialog.setConcept("saySmthDyn", "enu", [said])

                local_manager.dialog.activateTopic(PresentPerson.topic_name)

                local_manager.dialog.activateTag("saySmthTag", PresentPerson.topic_name)
                local_manager.dialog.gotoTag("saySmthTag", PresentPerson.topic_name)
                
            logger.log('Topic "' + PresentPerson.topic_name + '" loaded and activated', "Views Manager", logger.INFO)

    @staticmethod
    def received_data(local_manager, data):
        if hasattr(PresentPerson, 'topic_name'): # Means that a .top is loaded, need to check if it's done
            if data['data'] == "1":
                return {'id': PresentPerson.action_id}
        elif hasattr(PresentPerson, 'action_id'):
            return {'id': PresentPerson.action_id}
        return False

    @staticmethod
    def stop(local_manager):
        if hasattr(PresentPerson, 'topic_name') and PresentPerson.topic_name:
            local_manager.dialog.deactivateTopic(PresentPerson.topic_name)
            local_manager.dialog.unloadTopic(PresentPerson.topic_name)
            if hasattr(PresentPerson, 'reactivateMovement'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", True)
                delattr(PresentPerson, 'reactivateMovement')
            delattr(PresentPerson, "topic_name")