import json
import os
from tools import logger
from tools import arg_fetcher


class AskName:
    @staticmethod
    def start(js_view_key, local_manager, arguments):
        AskName.action_id = arg_fetcher.get_argument(arguments, 'id')
        if not AskName.action_id:
            logger.log("Missing id in {0} action arguments".format(js_view_key), "Views Manager", logger.ERROR)
            local_manager.send_view_result(js_view_key, {'error': 400})
        
        args = arg_fetcher.get_argument(arguments, 'args')
        speech = arg_fetcher.get_argument(args, 'speech')
        
        text = arg_fetcher.get_argument(speech, 'title')
        said = arg_fetcher.get_argument(speech, 'said')
        
        
        names = arg_fetcher.get_argument(args, 'names')
        AskName.names = names
        
        if text:
            local_manager.memory.raiseEvent(local_manager.lm_config['currentView']['ALMemory'], json.dumps({
                'view': js_view_key,
                'data': {
                    'textToShow': text,
                    'names': names
                }
            }))
        
        if said:
            if arg_fetcher.get_argument(speech, 'noSpeechAnimated'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", False)
                AskName.reactivateMovement = True

            if arg_fetcher.get_argument(speech, 'noSpeechRecognition'):
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['error'],
                    'system': toolbar['system']['micro']
                }))

                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_return'])

                local_manager.dialog.setConcept("saySmthDyn", "enu", [said])

                AskName.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(AskName.topic_name)

                local_manager.dialog.activateTag("saySmthTag", AskName.topic_name)
                local_manager.dialog.gotoTag("saySmthTag", AskName.topic_name)

            else:
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['ok'],
                    'system': toolbar['system']['micro']
                }))

                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['ask_smth'])

                local_manager.dialog.setConcept("askSmthDyn", "enu", [said])
                local_manager.dialog.setConcept("choices", "enu", names)

                AskName.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(AskName.topic_name)

                local_manager.dialog.activateTag("askSmthTag", AskName.topic_name)
                local_manager.dialog.gotoTag("askSmthTag", AskName.topic_name)

            logger.log('Topic "' + AskName.topic_name + '" loaded and activated', "Views Manager", logger.INFO)

    @staticmethod
    def received_data(local_manager, data):
        logger.log("AskName: data = " + str(data), "Views Manager", logger.DEBUG)
        if hasattr(AskName, 'action_id') and hasattr(AskName, 'names') and data['data'] in AskName.names:
            return {'id': AskName.action_id, 'name': data['data']}
        return True

    @staticmethod
    def stop(local_manager):
        if hasattr(AskName, 'topic_name') and AskName.topic_name:
            local_manager.dialog.deactivateTopic(AskName.topic_name)
            local_manager.dialog.unloadTopic(AskName.topic_name)
            if hasattr(AskName, 'reactivateMovement'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", True)
                delattr(AskName, 'reactivateMovement')
            delattr(AskName, "topic_name")