import json
import os
from tools import logger
from tools import arg_fetcher


class GoTo:
    @staticmethod
    def start(js_view_key, local_manager, arguments):
        GoTo.action_id = arg_fetcher.get_argument(arguments, 'id')
        if not GoTo.action_id:
            logger.log("Missing id in {0} action arguments".format(js_view_key), "Views Manager", logger.ERROR)
            local_manager.send_view_result(js_view_key, {'error': 400})
        
        args = arg_fetcher.get_argument(arguments, 'args')
        speech = arg_fetcher.get_argument(args, 'speech')
        
        text = arg_fetcher.get_argument(speech, 'title')
        said = arg_fetcher.get_argument(speech, 'said')
        
        location = arg_fetcher.get_argument(args, 'location')

        if said:
            top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_return'])
            GoTo.topic_name = local_manager.dialog.loadTopic(top_path)
        
        if text:
            local_manager.memory.raiseEvent(local_manager.lm_config['currentView']['ALMemory'], json.dumps({
                'view': js_view_key,
                'data': {
                    'textToShow': text,
                    'location': location
                }
            }))

        if said:
            if arg_fetcher.get_argument(speech, 'noSpeechAnimated'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", False)
                GoTo.reactivateMovement = True
            
            toolbar = local_manager.lm_config['toolbarState']
            local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                'state': toolbar['state']['ok'],
                'system': toolbar['system']['micro']
            }))

            local_manager.dialog.setConcept("saySmthDyn", "enu", [said])

            local_manager.dialog.activateTopic(GoTo.topic_name)

            local_manager.dialog.activateTag("saySmthTag", GoTo.topic_name)
            local_manager.dialog.gotoTag("saySmthTag", GoTo.topic_name)

            logger.log('Topic "' + GoTo.topic_name + '" loaded and activated', "Views Manager", logger.INFO)

    @staticmethod
    def received_data(local_manager, data):
        if hasattr(GoTo, 'topic_name'): # Means that a .top is loaded, need to check if it's done
            if data['data'] == "1":
                return {'id': GoTo.action_id}
        elif hasattr(GoTo, 'action_id'):
            return {'id': GoTo.action_id}
        return False

    @staticmethod
    def stop(local_manager):
        if hasattr(GoTo, 'topic_name') and GoTo.topic_name:
            local_manager.dialog.deactivateTopic(GoTo.topic_name)
            local_manager.dialog.unloadTopic(GoTo.topic_name)
            if hasattr(GoTo, 'reactivateMovement'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", True)
                delattr(GoTo, 'reactivateMovement')
            delattr(GoTo, "topic_name")