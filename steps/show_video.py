import json
import os
from tools import logger
from tools import arg_fetcher


class ShowVideo:
    @staticmethod
    def start(js_view_key, local_manager, arguments):
        ShowVideo.action_id = arg_fetcher.get_argument(arguments, 'id')
        if not ShowVideo.action_id:
            logger.log("Missing id in {0} action arguments".format(js_view_key), "Views Manager", logger.ERROR)
            local_manager.send_view_result(js_view_key, {'error': 400})
        
        args = arg_fetcher.get_argument(arguments, 'args')
        speech = arg_fetcher.get_argument(args, 'speech')
        
        text = arg_fetcher.get_argument(speech, 'title')
        said = arg_fetcher.get_argument(speech, 'said')
        desc = arg_fetcher.get_argument(args, 'description')
        if desc:
            desc = desc.split(';')
        else:
            desc = [""]
        
        if text:
            local_manager.memory.raiseEvent(local_manager.lm_config['currentView']['ALMemory'], json.dumps({
                'view': js_view_key,
                
                'data': {
                    'textToShow': {
                        'title': text,
                        'description': desc
                    },
                    'video': arg_fetcher.get_argument(args, 'video')
                }
            }))

        if said:
            if arg_fetcher.get_argument(speech, 'noSpeechAnimated'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", False)
                ShowVideo.reactivateMovement = True
            
            if arg_fetcher.get_argument(speech, 'noSpeechRecognition'):
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['error'],
                    'system': toolbar['system']['micro']
                }))

                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_return'])

                local_manager.dialog.setConcept("saySmthDyn", "enu", [said])

                ShowVideo.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(ShowVideo.topic_name)

                local_manager.dialog.activateTag("saySmthTag", ShowVideo.topic_name)
                local_manager.dialog.gotoTag("saySmthTag", ShowVideo.topic_name)

            else:
                toolbar = local_manager.lm_config['toolbarState']
                local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                    'state': toolbar['state']['ok'],
                    'system': toolbar['system']['micro']
                }))

                top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_catch_next'])

                local_manager.dialog.setConcept("saySmthDyn", "enu", [said])
                
                ShowVideo.topic_name = local_manager.dialog.loadTopic(top_path)
                local_manager.dialog.activateTopic(ShowVideo.topic_name)
                
                local_manager.dialog.activateTag("saySmthTag", ShowVideo.topic_name)
                local_manager.dialog.gotoTag("saySmthTag", ShowVideo.topic_name)
            
            logger.log('Topic "' + ShowVideo.topic_name + '" loaded and activated', "Views Manager", logger.INFO)

    @staticmethod
    def received_data(local_manager, data):
        if hasattr(ShowVideo, 'action_id'):
            return {'id': ShowVideo.action_id}
        return True

    @staticmethod
    def stop(local_manager):
        if hasattr(ShowVideo, 'topic_name') and ShowVideo.topic_name:
            local_manager.dialog.deactivateTopic(ShowVideo.topic_name)
            local_manager.dialog.unloadTopic(ShowVideo.topic_name)
            if hasattr(ShowVideo, 'reactivateMovement'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", True)
                delattr(ShowVideo, 'reactivateMovement')
            delattr(ShowVideo, "topic_name")