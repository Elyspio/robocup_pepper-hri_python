import json
import os
from tools import logger
from tools import arg_fetcher


class SeatGuest:
    @staticmethod
    def start(js_view_key, local_manager, arguments):
        SeatGuest.action_id = arg_fetcher.get_argument(arguments, 'id')
        if not SeatGuest.action_id:
            logger.log("Missing id in {0} action arguments".format(js_view_key), "Views Manager", logger.ERROR)
            local_manager.send_view_result(js_view_key, {'error': 400})
        
        args = arg_fetcher.get_argument(arguments, 'args')
        speech = arg_fetcher.get_argument(args, 'speech')
        
        text = arg_fetcher.get_argument(speech, 'title')
        said = arg_fetcher.get_argument(speech, 'said')

        name = arg_fetcher.get_argument(speech, 'name')

        if said:
            if arg_fetcher.get_argument(speech, 'noSpeechAnimated'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", False)
                SeatGuest.reactivateMovement = True
            
            said = said.format(name=name)
            top_path = os.path.join(os.getcwd(), local_manager.lm_config['tops']['say_smth_and_return'])
            
        if text:
            text = text.format(name=name)
            local_manager.memory.raiseEvent(local_manager.lm_config['currentView']['ALMemory'], json.dumps({
                'view': js_view_key,
                'data': {
                    'textToShow': text
                }
            }))

        if said:
            toolbar = local_manager.lm_config['toolbarState']
            local_manager.memory.raiseEvent(toolbar['ALMemory'], json.dumps({
                'state': toolbar['state']['ok'],
                'system': toolbar['system']['micro']
            }))

            SeatGuest.topic_name = local_manager.dialog.loadTopic(top_path)

            local_manager.dialog.setConcept("saySmthDyn", "enu", [said])
            local_manager.dialog.activateTopic(SeatGuest.topic_name)

            local_manager.dialog.activateTag("saySmthTag", SeatGuest.topic_name)
            local_manager.dialog.gotoTag("saySmthTag", SeatGuest.topic_name)

            logger.log('Topic "' + SeatGuest.topic_name + '" loaded and activated', "Views Manager", logger.INFO)

    @staticmethod
    def received_data(local_manager, data):
        if hasattr(SeatGuest, 'topic_name'): # Means that a .top is loaded, need to check if it's done
            if data['data'] == "1":
                return {'id': SeatGuest.action_id}
        elif hasattr(SeatGuest, 'action_id'):
            return {'id': SeatGuest.action_id}
        return False

    @staticmethod
    def stop(local_manager):
        if hasattr(SeatGuest, 'topic_name') and SeatGuest.topic_name:
            local_manager.dialog.deactivateTopic(SeatGuest.topic_name)
            local_manager.dialog.unloadTopic(SeatGuest.topic_name)
            if hasattr(SeatGuest, 'reactivateMovement'):
                local_manager.autonomous_life.setAutonomousAbilityEnabled("SpeakingMovement", True)
                delattr(SeatGuest, 'reactivateMovement')
            delattr(SeatGuest, "topic_name")