from datetime import datetime
import os

log_dir = "logs"
log_file = os.path.join(log_dir, datetime.now().strftime(
    "%Y-%m-%d_%H:%M:%S") + ".log")

HEARTBEAT = 0
EVENT = 1
DEBUG = 2
INFO = 3
WARNING = 4
ERROR = 5
CRITICAL = 6

print_level = DEBUG

__RST = "\033[0;0m"
__RED = "\033[1;31m"
__BLUE = "\033[1;34m"
__CYAN = "\033[1;36m"
__YELLOW = "\033[1;93m"
__GREEN = "\033[0;32m"
__GRAY = "\033[1;90m"
__CRIT = "\033[0;97;41m"


def log(message, application, level=INFO, timestamp=None):
    if timestamp is None:
        timestamp = datetime.now()

    if level == HEARTBEAT:
        level_str_color = "{color_lvl}{level:5}{color_rst}".format(
            level="HEART", color_lvl=__GRAY, color_rst=__RST)
        level_str = "HEART"
    if level == EVENT:
        level_str_color = "{color_lvl}{level:5}{color_rst}".format(
            level="EVENT", color_lvl=__GRAY, color_rst=__RST)
        level_str = "EVENT"
    if level == DEBUG:
        level_str_color = "{color_lvl}{level:5}{color_rst}".format(
            level="DEBUG", color_lvl=__GREEN, color_rst=__RST)
        level_str = "DEBUG"
    elif level == INFO:
        level_str_color = "{color_lvl}{level:5}{color_rst}".format(
            level="INFO", color_lvl=__CYAN, color_rst=__RST)
        level_str = "INFO"
    elif level == WARNING:
        level_str_color = "{color_lvl}{level:5}{color_rst}".format(
            level="WARN", color_lvl=__YELLOW, color_rst=__RST)
        level_str = "WARNING"
    elif level == ERROR:
        level_str_color = "{color_lvl}{level:5}{color_rst}".format(
            level="ERROR", color_lvl=__RED, color_rst=__RST)
        level_str = "ERROR"
    elif level == CRITICAL:
        level_str_color = "{color_lvl}{level:5}{color_rst}".format(
            level="CRIT", color_lvl=__CRIT, color_rst=__RST)
        level_str = "CRITICAL"

    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)

    with open(log_file, mode='a') as file:
        file.write("[{}] [{:5}] [{}]: {}".format(
            timestamp, level_str, application, message) + '\n')

    application = __BLUE + application + __RST
    timestamp = __BLUE + str(timestamp) + __RST

    message = "[{timestamp}] [{level}] [{application}]: {message}".format(
        timestamp=timestamp, level=level_str_color, application=application, message=message)

    if level >= print_level:
        print(message)


def set_print_level(level):
    global print_level
    print_level = level
