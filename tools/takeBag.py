import qi


######### Command to Test
## rosservice call /point_at "{x: 10.0, y: 10.0, z: 0.0, move_head: true, move_arm: true, pose_duration: 8.0}" 
## rosservice call /move_arm_hand "{arm_l_or_r: 'l',turn_rad: 0.0,stiffness: 0.8}"
## rosservice call /go_to_carry_pose "{arm_l_or_r:'l', keep_pose: true, stiffness: 0.8}"
#########


class MoveTurnRobot:
    _session=None
    _memory=None

    def __init__(self, ip, port):
        self._ip = ip
        self._port = port
        self.configureNaoqi()
        

    def configureNaoqi(self):
        self._app = qi.Application(["app_name", "--qi-url=tcp://" + self._ip + ":" + str(self._port)])
        self._app.start()
        self._session = self._app.session
        self._motion = self._session.service("ALMotion")
        

    def moveArmHand(self, req):
        self.release_fix_pose_thread()
        # go to an init head pose.
        if req['arm_l_or_r'] == "r":
            names = ["RShoulderPitch","RShoulderRoll","RElbowRoll","RElbowYaw","RWristYaw"]
            name_hand = "RHand"
            name_stiffness = ["RArm"]
            angles = [req['turn_rad'], -0.09, 0.26, -0.14, 1.74]
        else:
            names = ["LShoulderPitch","LShoulderRoll","LElbowRoll","LElbowYaw","LWristYaw"]
            name_hand = "LHand"
            name_stiffness = ["LArm"]
            angles = [req['turn_rad'], -0.09, 0.26, 0.035, -1.17]

        self._motion.stiffnessInterpolation(name_stiffness, 1.0, 1.0)

        times = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
        isAbsolute = True
        self._motion.angleInterpolation(names, angles, times, isAbsolute)
        self._motion.angleInterpolation(name_hand, 0.83, 1.0, True)

        self._motion.stiffnessInterpolation(name_stiffness, req['stiffness'], 0.2)


    def gotoCarryPose(self, req):
        #FIXME provide a seconde carry pose close to the robot legs
        leftArmEnable = True
        rightArmEnable = True
        if req['arm_l_or_r'] == "r":
            name_stiffness = ["RArm"]
            self._motion.stiffnessInterpolation(name_stiffness, 1.0, 1.0)
            self._motion.angleInterpolation("RHand", 0.0, 1.0, True)
            self._motion.angleInterpolation("RWristYaw", 1.73, 1.0, True)
            self._motion.angleInterpolation("RElbowYaw",  2.08, 1.0, True)
            self._motion.angleInterpolation("RElbowRoll", 1.48, 1.0, True)
            self._motion.angleInterpolation("RShoulderPitch",-0.47, 2.0, True)
            self._motion.angleInterpolation("RShoulderRoll", -0.09, 2.0, True)
            rightArmEnable = False
        else:
            name_stiffness = ["LArm"]
            #FIXME to be tested and modify !!!!
            self._motion.stiffnessInterpolation(name_stiffness, 1.0, 1.0)
            self._motion.angleInterpolation("LHand", 0.0, 1.0, True)
            self._motion.angleInterpolation("LWristYaw", -1.73, 1.0, True)
            self._motion.angleInterpolation("LElbowYaw", -2.08, 1.0, True)
            self._motion.angleInterpolation("LElbowRoll", -1.48, 1.0, True)
            self._motion.angleInterpolation("LShoulderPitch", -0.47, 2.0, True)
            self._motion.angleInterpolation("LShoulderRoll", -0.09, 2.0, True)
            leftArmEnable = False
        if req['keep_pose']:
            self._motion.setMoveArmsEnabled(leftArmEnable, rightArmEnable)

        self._motion.stiffnessInterpolation(name_stiffness, req['stiffness'], 0.2)


    def release_fix_pose_thread(self):
        self._motion.setMoveArmsEnabled(True, True)

if __name__ == "__main__":
    ip = "localhost"
    port = 9559
   
    motion = MoveTurnRobot(ip, port)
    motion.moveArmHand({'arm_l_or_r': 'r', 'turn_rad': 0.5, 'stiffness': 0.8})
    print "a"
    motion.moveArmHand({'arm_l_or_r': 'r', 'turn_rad': 0.0, 'stiffness': 0.8})
    print "b"
    motion.gotoCarryPose({'arm_l_or_r':'r', 'keep_pose': True, 'stiffness': 0.8})
    print "c"
    