from naoqi import *
import time

check = 0

# create python module
class myModule(ALModule):
  """python class myModule test auto documentation: comment needed to create a new python module"""


  def pythondatachanged(self, strVarName, value, strMessage):
    """callback when data change"""
    print "datachanged", strVarName, " ", value, " ", strMessage
    global check
    check = 1

  def _pythonPrivateMethod(self, param1, param2, param3):
    global check

broker = ALBroker("pythonBroker", "localhost", 9999, "localhost", 9559)

# call method
try:
  prox = ALProxy("ALMemory")
  i = 0
  dataList = prox.getDataListName()
  dataList.sort()
  for a in dataList:
    print a, "\t", prox.getData(a)
    # i += 1
    # if i > 20:
    #   i = 0
    #   raw_input()
  exit(0)
  pythonModule = myModule("pythonModule")
  prox = ALProxy("ALMemory")
  #prox.insertData("val",1) # forbidden, data is optimized and doesn't manage callback
  prox.subscribeToEvent("TouchChanged", "pythonModule", "pythondatachanged") # event is case sensitive !

except Exception,e:
  print "error"
  print e
  exit(1)

while (1):
  time.sleep(2)

